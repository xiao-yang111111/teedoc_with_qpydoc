
---
title: QuecPython
keywords: QuecPython, markdown, Quectel, 物联网, 移远, 静态网站, 文档中心
desc: QuecPython, 使用教程，快速开发的新语言
id: home_page
---

<div id="home_page">
    <div>
        <h1><span>QuecPython文档中心</span></h1>
        <h2>让开发更简单迅速</h3>
    </div>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="static/css/just-the-docs.css">
    <link rel="stylesheet" href="static/css/izmir.css">
    <link rel="stylesheet" href="static/css/docs.css">
    <link rel="stylesheet" href="static/css/fontawesome.min.css">
    <link rel="stylesheet" href="static/css/fa-solid.min.css">
    <script type="text/javascript" src="static/js/just-the-docs.js"></script>
    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?4a99be573097e5c181db4406516f1bca";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <div id="main-content" class="home-content home-wrapper" role="main">
        <!-- 第一行-->
        <div class="col-3">
            <!-- QuecPython API 接口文档-->
            <div class="col-item">
                <figure class="c4-izmir c4-border-corners-1 c4-image-zoom-out c4-gradient-bottom" tabindex="0"
                    style="--primary-color: #E0EAFC; --secondary-color: #CFDEF3; --text-color: #1f467b; --border-color: #1f467b; --image-opacity: .1;">
                    <img src="static/image/image01.jpg" alt="Sample Image" />
                    <a href="/API_reference/zh/index.html" class="article-a">
                        <figcaption class="c4-layout-top-left">
                            <div class="c4-reveal-down">
                                <div>
                                    <h2 style="margin-bottom: 5px;"></h2>
                                    <p>详解QuecPython API接口描述,包括API参考用例等</p>
                                </div>
                            </div>
                        </figcaption>
                </figure>
                <a href="/API_reference/zh/index.html" class="article-a">
                    <p class="article-p-h">API参考手册</p>
                </a>
                <p class="article-p-t">最新：2023-01-19</font>
                </p>
            </div>
            <!-- QuecPython开发指南-->
            <div class="col-item">
                <figure class="c4-izmir c4-border-corners-1 c4-image-zoom-out c4-gradient-bottom" tabindex="0"
                    style="--primary-color: #E0EAFC; --secondary-color: #CFDEF3; --text-color: #1f467b; --border-color: #1f467b; --image-opacity: .1;">
                    <img src="static/image/image02.jpg" alt="Sample Image" />
                    <a href="/development_guide/zh/index.html" class="article-a">
                        <figcaption class="c4-layout-top-left">
                            <div class="c4-reveal-down">
                                <div>
                                    <h2 style="margin-bottom: 5px;"></h2>
                                    <p>QuecPython开发指南,包括BSP、网络通信、外设应用开发等</p>
                                </div>
                            </div>
                        </figcaption>
                </figure>
                <a href="/development_guide/zh/index.html" class="article-a">
                    <p class="article-p-h">开发指南</p>
                </a>
                <p class="article-p-t">最新：2023-01-16</font>
                </p>
            </div>
            <!-- QuecPython开发工具使用教程-->
            <div class="col-item">
                <figure class="c4-izmir c4-border-corners-1 c4-image-zoom-out c4-gradient-bottom" tabindex="0"
                    style="--primary-color: #E0EAFC; --secondary-color: #CFDEF3; --text-color: #1f467b; --border-color: #1f467b; --image-opacity: .1;">
                    <img src="static/image/image03.jpg" alt="Sample Image" />
                    <a href="/development_tool_tutorial/zh/index.html" class="article-a">
                        <figcaption class="c4-layout-top-left">
                            <div class="c4-reveal-down">
                                <div>
                                    <h2 style="margin-bottom: 5px;"></h2>
                                    <p>QuecPython开发工具使用教程</p>
                                </div>
                            </div>
                        </figcaption>
                </figure>
                <a href="/development_tool_tutorial/zh/index.html" class="article-a">
                    <p class="article-p-h">工具使用教程</p>
                </a>
                <p class="article-p-t">最新：2023-01-16</font>
                </p>
            </div>
        </div>
        <!-- 第二行-->
        <div class="col-3">
            <!-- QuecPython高级教程-->
            <div class="col-item">
                <figure class="c4-izmir c4-border-corners-1 c4-image-zoom-out c4-gradient-bottom" tabindex="0"
                    style="--primary-color: #E0EAFC; --secondary-color: #CFDEF3; --text-color: #1f467b; --border-color: #1f467b; --image-opacity: .1;">
                    <img src="static/image/image05.jpg" alt="Sample Image" />
                    <a href="/advanced_tutorial/zh/index.html" class="article-a">
                        <figcaption class="c4-layout-top-left">
                            <div class="c4-reveal-down">
                                <div>
                                    <h2 style="margin-bottom: 5px;"></h2>
                                    <p>QuecPython应用框架、云平台对接、图形化界面开发等</p>
                                </div>
                            </div>
                        </figcaption>
                </figure>
                <a href="/advanced_tutorial/zh/index.html" class="article-a">
                    <p class="article-p-h">高级教程</p>
                </a>
                <p class="article-p-t">最新：2023-01-16</font>
                </p>
            </div>
            <!-- QuecPython解决方案-->
            <div class="col-item">
                <figure class="c4-izmir c4-border-corners-1 c4-image-zoom-out c4-gradient-bottom" tabindex="0"
                    style="--primary-color: #E0EAFC; --secondary-color: #CFDEF3; --text-color: #1f467b; --border-color: #1f467b; --image-opacity: .1;">
                    <img src="static/image/image06.jpg" alt="Sample Image" />
                    <a href="/solutions/zh/index.html" class="article-a">
                        <figcaption class="c4-layout-top-left">
                            <div class="c4-reveal-down">
                                <div>
                                    <h2 style="margin-bottom: 5px;"></h2>
                                    <p>云喇叭、DTU、对讲机、Tracker和BMS相关等solution介绍</p>
                                </div>
                            </div>
                        </figcaption>
                </figure>
                <a href="/solutions/zh/index.html" class="article-a">
                    <p class="article-p-h">解决方案</p>
                </a>
                <p class="article-p-t">最新：2023-01-16</font>
                </p>
            </div>
			<!-- QuecPython常见问题-->
			<div class="col-item">
				<figure class="c4-izmir c4-border-corners-1 c4-image-zoom-out c4-gradient-bottom" tabindex="0"
					style="--primary-color: #E0EAFC; --secondary-color: #CFDEF3; --text-color: #1f467b; --border-color: #1f467b; --image-opacity: .1;">
					<img src="static/image/image08.jpg" alt="Sample Image" />
					<a href="/FAQ/zh/index.html" class="article-a">
						<figcaption class="c4-layout-top-left">
							<div class="c4-reveal-down">
								<div>
									<h2 style="margin-bottom: 5px;"></h2>
									<p>常见问题,刷砖救砖,文档系统编写规范,
										参与文档贡献流程,提交文档错误流程。</p>
								</div>
							</div>
						</figcaption>
				</figure>
				<a href="/FAQ/zh/index.html" class="article-a">
					<p class="article-p-h">FAQ</p>
				</a>
				<p class="article-p-t">最新：2023-01-16</font>
				</p>
			</div>
		</div>
	</div>		
</div>
