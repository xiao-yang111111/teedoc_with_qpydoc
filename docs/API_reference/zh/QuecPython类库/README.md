# QuecPython 类库

> QuecPython 类库是针对移远通信模组特有功能而开发的，包括电话、短信、网络、多媒体等各种功能。

本文描述了 QuecPython 类库中的模块。

## QuecPython 类库列表

- [example - 执行Python脚本](./example.md)
- [dataCall - 数据拨号](./dataCall.md)
- [cellLocator - 基站定位](./cellLocator.md)
- [wifilocator - WiFi定位](./wifilocator.md)
- [atcmd - 发送AT指令](./atcmd.md)
- [machine - 硬件相关功能](./machine.md)
- [ethernet - 以太网相关功能](./ethernet.md)
- [...](./....md)

<!--参考https://python.quectel.com/wiki/#/zh-cn/api/QuecPythonClasslib进行补充-->
