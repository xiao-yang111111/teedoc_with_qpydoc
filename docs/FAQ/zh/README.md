# QuecPython-FAQ

QuecPython-FAQ 是由QuecPython团队推出的针对常见问题的总结。在线帮助我们的用户快速检索经常问到的问题，通过简单的解释获得解答。目前常见问题的种类涵盖：开发环境与工具、应用方案、软件平台、硬件相关和测试测试。

||||
|---|---|---|
|![](./media/instruction.png)|![](./media/development-environment.png)|![](./media/application-solution.png)|
|[使用说明](./使用说明/README.md)|[开发环境与工具](./开发环境与工具/README.md)|[应用方案](./应用方案/README.md)|
|![](./media/software-framework.png)|![](./media/hardware-related.png)|![](./media/test-verification.png)|
|[软件平台](./软件平台/README.md)|[硬件相关](./硬件相关/README.md)|[测试校验](./测试校验/README.md)|
