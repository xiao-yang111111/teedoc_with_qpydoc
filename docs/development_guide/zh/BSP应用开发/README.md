# BSP应用开发

BSP应用开发主要介绍模组自带外设功能的基础知识及入门测试操作，例如GPIO、UART等。部分测试需要依赖外设设备，本部分内容主要介绍基本概念和理论，实际操作验证通过文章最后的链接跳转到外设应用文档部分。

# BSP应用开发文档目录

- [BSP-Audio应用开发](BSP-Audio应用开发.md)
- [BSP-GPIO应用开发](BSP-GPIO应用开发.md)
- [BSP-ExtInt引脚中断应用开发](BSP-ExtInt引脚中断应用开发.md)
- [BSP-UART应用开发](BSP-UART应用开发.md)
- [BSP-IIC(I2C)应用开发](BSP-IIC(I2C)应用开发.md)
- [BSP-SPI应用开发](BSP-SPI应用开发.md)
- [BSP-ADC应用开发](BSP-ADC应用开发.md)
- [BSP-PWM应用开发](BSP-PWM应用开发.md)
- [BSP-Timer应用开发](BSP-Timer应用开发.md)
- [BSP-RTC应用开发](BSP-RTC应用开发.md)
- [BSP-WDT看门狗应用开发](BSP-WDT看门狗应用开发.md)